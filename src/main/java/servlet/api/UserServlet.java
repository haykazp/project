package servlet.api;



import com.google.gson.Gson;
import domain.User;
import service.implementions.UserDaoImplementation;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


public class UserServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        UserDaoImplementation userDaoImplementation = new UserDaoImplementation();
        List<User> users = userDaoImplementation.getAll();

        response.setContentType("application/json; charset=utf-8");

        PrintWriter writer = response.getWriter();
        writer.print(new Gson().toJson(users));
        writer.flush();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {



    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response)  {

    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {

    }





}
