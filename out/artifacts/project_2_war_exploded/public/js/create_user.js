$(document).ready(function(){

    var user_first_name = $('#first_name').val();
    var user_last_name = $('#last_name').val();
    var user_email = $('#email').val();
    var user_password = $('#password').val();
    var user_password_repeat = $('#password_repeat').val();



    var jsonObject = {
        first_name: user_first_name,
        last_name: user_last_name,
        email: user_email,
        password: user_password,
        password_repeat:user_password_repeat
    };


    $('#employee').submit(function(e){


        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/register',
            data: JSON.stringify(jsonObject),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function (data) {
                console.log("ok");
            }
        });

    });


});